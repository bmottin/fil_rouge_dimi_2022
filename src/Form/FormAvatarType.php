<?php

namespace App\Form;

use App\Entity\UserProfile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class FormAvatarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('avatarName', HiddenType::class)
            ->add('avatarFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => true, // autoriser la suppression de l'image
                'delete_label' => "supprimer image",
                'download_label' => "Télécharger",
                'download_uri' => true,
                'asset_helper' => true
              ])
            ->add("submit", SubmitType::class, [
                'label' => "Changer avatar"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserProfile::class,
        ]);
    }
}
