<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('author')
            ->add('content', CKEditorType::class, [
                "config_name" => 'test'
            ] )
            ->add('imageName', HiddenType::class)
            ->add('imageFile', VichImageType::class, [
              'required' => false,
              'allow_delete' => true, // autoriser la suppression de l'image
              'delete_label' => "supprimer image",
              'download_label' => "Télécharger",
              'download_uri' => true,
              'asset_helper' => true
            ]
            )
            ->add('status')
            ->add('slug')
            ->add('category')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
