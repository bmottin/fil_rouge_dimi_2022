<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }


    /* ---------------------------------------
        Queryy Builder
     ---------------------------------------- */
    /**
     * @return Article[] Returns an array of Article objects
     */
    public function findByStatusQB($value)
    {
        return $this->createQueryBuilder('article')
            ->innerJoin('article.status', 'status')
            ->where('status.internalName = :val')
            ->setParameter('val', $value)
            ->orderBy('article.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

      /**
     * @return Article[] Returns an array of Article objects
     */
    public function findByAuthorQB($value)
    {
/*
        $arr = [
            [
                "2020" => [
                    "u1"=> 20,
                    "u2"=> 30,
                    "u3"=> 10,
                ],
                "2021" => [
                    "u1"=> 20,
                    "u2"=> 30,
                    "u3"=> 10,
                ],
                "2022" => [
                    "u1"=> 20,
                    "u2"=> 30,
                    "u3"=> 10,
                ],
            ]
        ]
        //*/

        return $this->createQueryBuilder('article')
            ->innerJoin('article.author', 'user')
            ->where('user.email = :val')
            // ->setParameter('val', $value)
            ->orderBy('article.updatedAt', 'ASC')
            ->setParameters(['val'=> $value])
            ->getQuery()
            ->getResult()
        ;
    }


    /* ---------------------------------------
        DQL Doctrine Query Language
     ---------------------------------------- */

     public function findByAuthorDQL($value ) {
        $entityManager = $this->getEntityManager();
        
        $query = $entityManager->createQuery(
            "SELECT article 
            FROM \APP\Entity\Article article
            INNER JOIN article.author user
            WHERE user.email = :val"
        )->setParameter('val', $value) ;

        return $query->getResult();
     }

   /* ---------------------------------------
        Native Query
     ---------------------------------------- */

     function findByCategoryNative($value, $limit = 10) {
        $entityManager = $this->getEntityManager();

        $rsm = new ResultSetMappingBuilder($entityManager);
        $rsm->addRootEntityFromClassMetadata("\App\Entity\Article", 'article');

        $query = $entityManager->createNativeQuery(
            "
                SELECT article.* 
                FROM article
                INNER JOIN category 
                ON article.category_id = category.id
                WHERE category.label = ?
                limit ?
            ", $rsm
        )->setParameters([1 => $value, 2 => $limit]);

        return $query->getResult();

     }

     function findByYear($value) {
        $entityManager = $this->getEntityManager();

        $rsm = new ResultSetMappingBuilder($entityManager);
        $rsm->addRootEntityFromClassMetadata("\App\Entity\Article", 'article');

        $query = $entityManager->createNativeQuery(
            "
            SELECT 
                article.*
            FROM 
                article 
            INNER JOIN 
                user 
            ON 
                article.author_id = user.id 
            WHERE YEAR(created_at) = ? 
                
            ", $rsm
        )->setParameters([1 => $value]);

        return $query->getResult();

     }

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
