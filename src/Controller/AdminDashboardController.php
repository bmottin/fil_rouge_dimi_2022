<?php

namespace App\Controller;

use Symfony\UX\Chartjs\Model\Chart;
use App\Repository\StatusRepository;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminDashboardController extends AbstractController
{
    /**
     * @Route("/admin/", name="app_admin_dashboard")
     */
    public function index(ArticleRepository $articleRepository, UserRepository $userRepository, ChartBuilderInterface $chartBuilder): Response
    {

        $authors = $userRepository->findAll();

        $nbPostsByAuthors = [];
        foreach ($authors as $key => $author) {
            // dump(gettype($author->getEmail()));
            $nbPostsByAuthors[$author->getEmail()] = count($author->getArticles());
        }

        $articles2020 = $this->getArticleByYears(2020, $authors, $articleRepository);
        $articles2021 = $this->getArticleByYears(2021, $authors, $articleRepository);
        $articles2022 = $this->getArticleByYears(2022, $authors, $articleRepository);


        //dd($articles2021);
        /*
        $nbPostsByAuthors2021 = [];
        foreach ($authors as $key => $author) {
            $nbPostsByAuthors2021[$author->getEmail()] = 0;
        }
        foreach ($articles2021 as $key => $article) {
            // dump(gettype($author->getEmail()));
             if(array_key_exists($article->getAuthor()->getEmail(), $nbPostsByAuthors2021)){
                $nbPostsByAuthors2021[$article->getAuthor()->getEmail()]++;
             } else {
                $nbPostsByAuthors2021[$article->getAuthor()->getEmail()] = 1;
             }
        } //*/
        dump($nbPostsByAuthors);
        // dd($nbPostsByAuthors2021);

        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);

        $chart->setData(
            [
                'labels' => array_keys($nbPostsByAuthors),
                'datasets' => [
                    [
                        'label' => 'Nb de poste /auteur ',
                        'backgroundColor' => 'rgb(255, 99, 132)',
                        'borderColor' => 'rgb(255, 99, 132)',
                        'data' => array_values($nbPostsByAuthors),
                    ],
                    [
                        'label' => 'Nb de poste /auteur 2021',
                        'backgroundColor' => 'rgb(255, 99, 0)',
                        'borderColor' => 'rgb(255, 99, 0)',
                        'data' => array_values($articles2020),
                    ],
                    [
                        'label' => 'Nb de poste /auteur 2021',
                        'backgroundColor' => 'rgb(0, 99, 132)',
                        'borderColor' => 'rgb(0, 99, 132)',
                        'data' => array_values($articles2021),
                    ],
                    [
                        'label' => 'Nb de poste /auteur 2022',
                        'backgroundColor' => 'rgb(255, 0, 132)',
                        'borderColor' => 'rgb(255, 0, 132)',
                        'data' => array_values($articles2022),
                    ],
                ],
            ],
        );

        $chart->setOptions([
            'scales' => [
                'y' => [
                    'suggestedMin' => 0,
                    'suggestedMax' => 100,
                ],
            ],
        ]);

        return $this->render('admin_dashboard/index.html.twig', [
            'active' => 'nav_dashboard',
            'articlesToPublish' => $articleRepository->findByStatusQB('draft'),
            'articlesPublished' => $articleRepository->findByStatusQB('publish'),
            //'articleByAuthor' => $articleRepository->findByAuthorQB('u2@mail.com'),
            'articleByAuthor' => $articleRepository->findByAuthorDQL('u2@mail.com'),
            'articleByCategory' => $articleRepository->findByCategoryNative('materiel', 20),

            'chart' => $chart
        ]);
    }

    function getArticleByYears($year, $authors, ArticleRepository $articleRepository)
    {
        $articles = $articleRepository->findByYear($year);
        $nbPostsByAuthors = [];

        foreach ($authors as $key => $author) {
            $nbPostsByAuthors[$author->getEmail()] = 0;
        }
        foreach ($articles as $key => $article) {
            $nbPostsByAuthors[$article->getAuthor()->getEmail()]++;
        }

        return $nbPostsByAuthors;
    }
}
