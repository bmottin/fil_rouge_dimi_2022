<?php

namespace App\Controller;

use App\Entity\UserProfile;
use App\Form\FormAvatarType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserProfileController extends AbstractController
{
    /**
     * @Route("/user/profile/{id}", name="app_user_show_profile")
     */
    public function showProfile(UserProfile $userProfile): Response
    {
        $canEdit = false;

        if ($this->getUser() == $userProfile->getUser()) {
            dump("Yatta");
            $canEdit = true;
        }

        return $this->render('user_profile/show.html.twig', [
            'user_profile' => $userProfile,
            'can_edit' => $canEdit
        ]);
    }

    /**
     * @Route("/user/profile/{id}/edit", name="app_user_edit_profile")
     */
    public function editProfile(UserProfile $userProfile, Request $request, EntityManagerInterface $manager): Response
    {

        // controlle que l'utilisateur actuel est le propriétaire du profil
        if ($this->getUser() != $userProfile->getUser()) {
            // si c'est pas le cas, message erreur
            $this->addFlash('warning', "Opération interdite");

            // redirige sur la page accueil
            return $this->redirectToRoute('home');
        }

        $formAvatar = $this->createForm(FormAvatarType::class, $userProfile);

        $formAvatar->handleRequest($request);

        if ($formAvatar->isSubmitted() && $formAvatar->isValid()) {

            
            $userProfile->setUpdatedAt(new \Datetime());

            $manager->persist($userProfile);
            $manager->flush();

            $this->addFlash("success", "Votre avatar a été Mis à jour");

            return $this->redirectToRoute('blog');
        }

        return $this->render('user_profile/edit.html.twig', [
            'user_profile' => $userProfile,
            'form_avatar' => $formAvatar->createView()
        ]);
    }
}
