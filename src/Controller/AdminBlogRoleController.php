<?php

namespace App\Controller;

use App\Entity\BlogRole;
use App\Form\BlogRoleType;
use App\Repository\BlogRoleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/blog/role")
 */
class AdminBlogRoleController extends AbstractController
{
    /**
     * @Route("/", name="app_admin_blog_role_index", methods={"GET"})
     */
    public function index(BlogRoleRepository $blogRoleRepository): Response
    {
        return $this->render('admin_blog_role/index.html.twig', [
            'blog_roles' => $blogRoleRepository->findAll(),
            'active' => 'nav_blog_role'
        ]);
    }

    /**
     * @Route("/new", name="app_admin_blog_role_new", methods={"GET", "POST"})
     */
    public function new(Request $request, BlogRoleRepository $blogRoleRepository): Response
    {
        $blogRole = new BlogRole();
        $form = $this->createForm(BlogRoleType::class, $blogRole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $blogRoleRepository->add($blogRole, true);

            return $this->redirectToRoute('app_admin_blog_role_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_blog_role/new.html.twig', [
            'blog_role' => $blogRole,
            'form' => $form,
            'active' => 'nav_blog_role'
        ]);
    }

    /**
     * @Route("/{id}", name="app_admin_blog_role_show", methods={"GET"})
     */
    public function show(BlogRole $blogRole): Response
    {
        return $this->render('admin_blog_role/show.html.twig', [
            'blog_role' => $blogRole,
            'active' => 'nav_blog_role'
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_admin_blog_role_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, BlogRole $blogRole, BlogRoleRepository $blogRoleRepository): Response
    {
        $form = $this->createForm(BlogRoleType::class, $blogRole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $blogRoleRepository->add($blogRole, true);

            return $this->redirectToRoute('app_admin_blog_role_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_blog_role/edit.html.twig', [
            'blog_role' => $blogRole,
            'form' => $form,
            'active' => 'nav_blog_role'
        ]);
    }

    /**
     * @Route("/{id}", name="app_admin_blog_role_delete", methods={"POST"})
     */
    public function delete(Request $request, BlogRole $blogRole, BlogRoleRepository $blogRoleRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$blogRole->getId(), $request->request->get('_token'))) {
            $blogRoleRepository->remove($blogRole, true);
        }

        return $this->redirectToRoute('app_admin_blog_role_index', [], Response::HTTP_SEE_OTHER);
    }
}
