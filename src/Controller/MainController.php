<?php

namespace App\Controller;

use App\Repository\BlogRepository;
use App\Repository\CustomPageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController {
    /**
     * @Route("/", name="home")
     */
    public function home(BlogRepository $blogRepository): Response {
        // je recupere l'instance l'application avec l'id 1
        $blog = $blogRepository->findAll();

        if($blog){
            $slogan = $blog[0]->getSlogan();
        } else {
            $slogan = null;
        }
        return $this->render('main/home.html.twig', [
            'firstname' => 'Benoit',
            'age' => 17,
            'slogan' => $slogan
        ]);
    }

    /**
     * @Route("/cgu", name="cgu")
     */
    public function cgu(): Response {
        return $this->render('main/cgu.html.twig', [
            'content' => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odit quia quas aperiam, maxime sed voluptates repellat, inventore alias iste amet veritatis saepe. Sint rerum doloremque deserunt repellendus cum! Ex, odio?",
            'tldr' => "En gros c'est des CGU quoi!",
            "is_lazy" => false
        ]);
    }

    /**
     * @Route("/page/{url}", name="show_custom_page")
     */
    public function showCustomPage($url, CustomPageRepository $customPageRepository): Response {
        $page = $customPageRepository->findOneBy(['url' => $url]);
        //$page = $customPageRepository->findBy(['url' => $url]);

        return $this->render('main/page.html.twig', [
            'page' => $page
        ]);
    }
}
