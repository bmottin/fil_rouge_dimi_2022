<?php

namespace App\Controller;

use App\Entity\BlogRole;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\BlogRoleRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/user")
 */
class AdminUserController extends AbstractController {
    /**
     * @Route("/", name="app_admin_user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response {
        return $this->render('admin_user/index.html.twig', [
            'users' => $userRepository->findAll(),
            'active' => 'nav_user'
        ]);
    }

    /**
     * @Route("/new", name="app_admin_user_new", methods={"GET", "POST"})
     */
    public function new(Request $request, UserRepository $userRepository, BlogRoleRepository $brRepo): Response {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $roleIds = $request->request->get('user')['roles']; // nous recuperons la valeurs des checkbox roles
            $roles = []; // je creer un tableau vide, qui va contruire mon tableau d'internal name (voir dans la fixture comment nous avons attribuer des roles à nos utilisateurs)

            foreach ($roleIds as $key => $i) { // pour chaque id recup // j'ai un warning de VSC, c'est un faux positif à cause du get
                $role = $brRepo->find($i); // je trouve le role qui correspond
                $roles[] = $role->getInternalName(); // et j'ajoute dans mon tableau de role la propriété "internalName"
            }

            $user->setRoles($roles); // une fois tous les roles recup, je l'ajoute à mon user
            $userRepository->add($user, true); // nouvelle méthode pour persister une entité
            return $this->redirectToRoute('app_admin_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_user/new.html.twig', [
            'user' => $user,
            'form' => $form,
            'active' => 'nav_user'
        ]);
    }

    /**
     * @Route("/{id}", name="app_admin_user_show", methods={"GET"})
     */
    public function show(User $user): Response {
        return $this->render('admin_user/show.html.twig', [
            'user' => $user,
            'active' => 'nav_user'
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_admin_user_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, User $user, UserRepository $userRepository, BlogRoleRepository $brRepo): Response {

        $roles = []; // definition d'un tableau vide, que je vais peupler avec mon BlogRole

        foreach ($user->getRoles() as $key => $role) { // pour chaque role de l'urilisateur
            $roles[] = $brRepo->findOneBy(['internalName' => $role]); // recupere la correspondance entre le role enregitrer et mes BlogRole
             
        }
        $user->setRoles($roles); // j'écrase les roles stockés avec mon tableau instance de BlogRole (pas de risque tant que je persiste pas)
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $roleIds = $request->request->get('user')['roles']; // nous recuperons la valeurs des checkbox roles
            $roles = []; // je creer un tableau vide, qui va contruire mon tableau d'internal name (voir dans la fixture comment nous avons attribuer des roles à nos utilisateurs)


            foreach ($roleIds as $key => $i) { // pour chaque id recup // j'ai un warning de VSC, c'est un faux positif à cause du get
                $role = $brRepo->find($i); // je trouve le role qui correspond
                $roles[] = $role->getInternalName(); // et j'ajoute dans mon tableau de role la propriété "internalName"
            }

            $user->setRoles($roles); // une fois tous les roles recup, je l'ajoute à mon user


            $userRepository->add($user, true);

            return $this->redirectToRoute('app_admin_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_user/edit.html.twig', [
            'user' => $user,
            'form' => $form,
            'active' => 'nav_user'
        ]);
    }

    /**
     * @Route("/{id}", name="app_admin_user_delete", methods={"POST"})
     */
    public function delete(Request $request, User $user, UserRepository $userRepository): Response {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $userRepository->remove($user, true);
        }

        return $this->redirectToRoute('app_admin_user_index', [], Response::HTTP_SEE_OTHER);
    }

    function getRoleByBlogRole() {
    }
}
