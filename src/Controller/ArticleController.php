<?php

namespace App\Controller;

use App\Entity\Alike;
use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use App\Form\CommentType;
use App\Repository\AlikeRepository;
use App\Repository\StatusRepository;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArticleController extends AbstractController {
    /**
     * @Route("/article", name="article")
     */
    public function article(ArticleRepository $articleRepo, StatusRepository $statusRepository): Response {
        // $articleRepo =  $this->getDoctrine()->getRepository(Article::class);
        $status = $statusRepository->findOneBy(['internalName' => 'publish']);

        // dump($articleRepo->findBy(['status' => $status ]));

        return $this->render('article/articles.html.twig', [
            'articles' => $articleRepo->findBy(['status' => $status ]),
        ]);
    }

    /**
     * @Route("/article/new", name="article_new")
     */
    public function article_new(Request $request, EntityManagerInterface $manager, StatusRepository $statusRepository): Response {

        $article = new Article();
        // je genere un formulaire avec formBuilder
        $form = $this->createFormBuilder($article)
            ->add('title', TextType::class, [
                'label' => "Titre",
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'monTitre'
                    ]
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Contenu',
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('author', TextType::class, [
                'label' => "Auteur",
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'auteur'
                    ]
            ])
            /*->add('imageName', TextType::class, [
                'label' => "image de l'article",
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'imgArt'
                    ]
            ])//*/
            ->add('category', EntityType::class, [
                'class' => \App\Entity\Category::class,
                "expanded" => false,
                "multiple" => false,
            ])

            ->add("submit", SubmitType::class, [
                "label" => "Ajouter un article",
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
            ->getForm();

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $status = $statusRepository->findOneBy(['internalName' => 'draft']);

                $article->setCreatedAt(new \DateTime());
                $article->setUpdatedAt(new \DateTime());

                $article->setStatus($status);

                $manager->persist($article);
                $manager->flush();

                return $this->redirectToRoute('article_show', ['id' => $article->getId()]);
            }

        return $this->render('article/new.html.twig', [
            'formArticle' => $form->createView(),
            'pageTitle' => "Ajouter un article"
        ]);
    }

 /**
     * @Route("/article/{id}/edit", name="article_edit")
     */
    public function article_edit(Article $article, Request $request, EntityManagerInterface $manager): Response {

        /*$images = [];
         $images[] = $article->getFirst //*/
        // $article = $articleRepo->find($id);
        // $article = new Article();
        // je genere un formulaire avec formBuilder
        $form = $this->createFormBuilder($article)
            ->add('title', TextType::class, [
                'label' => "Titre",
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'monTitre'
                    ]
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Contenu',
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('author', TextType::class, [
                'label' => "Auteur",
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'auteur'
                    ]
            ])
            /*->add('imageName', TextType::class, [
                'label' => "image de l'article",
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'imgArt'
                    ]
            ])//*/
            ->add('status', EntityType::class, [
                'class' => \App\Entity\Status::class,
            ])
            ->add('category', EntityType::class, [
                'class' => \App\Entity\Category::class,
                "expanded" => false,
                "multiple" => false,
            ])

            ->add("submit", SubmitType::class, [
                "label" => "Mettre à jour",
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
            ->getForm();

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $article->setUpdatedAt(new \DateTime());

                $manager->persist($article);
                $manager->flush();

                return $this->redirectToRoute('article_show', ['id' => $article->getId()]);
            }

        return $this->render('article/new.html.twig', [
            'formArticle' => $form->createView(),
            'pageTitle' => "Modifier un article"
        ]);
    }


     /**
     * @Route("/article/{id}/like", name="article_like")
     * // FIXME voir redirect
     */
    public function articleLike(Article $article, EntityManagerInterface $manager, AlikeRepository $alikeRepository): Response {

        $user = $this->getUser();

        // si l'utilisateur est connecté
        if($user) {
            // si l'article est déjà liker
            if($article->isAlikedByUser($user)) {
                // je supprime le like
                $alike = $alikeRepository->findOneByUserAndArticle($user->getUserProfile(), $article);
                $manager->remove($alike);
            } else {
                // je like
                $alike = new Alike();
                $alike->setArticle($article);
                $alike->setUserProfile($user->getUserProfile()); // Warning ?? FIXME voir cast
                $manager->persist($alike);
            }
            $manager->flush();

        } else {
            $this->addFlash("warning", "Vous devez être connecté pour liker un article");
            return $this->redirectToRoute('app_security_login');
        }

        return $this->redirectToRoute('article');
    }

   /**
     * @Route("/article/{id}/like_ajax", name="article_like_ajax")
     * // FIXME voir redirect
     */
    public function articleLikeAjax(Article $article, EntityManagerInterface $manager, AlikeRepository $alikeRepository): Response {

        $user = $this->getUser();

        // si l'utilisateur est connecté
        if($user) {
            // si l'article est déjà liker
            
            if($article->isAlikedByUser($user)) {
                // je supprime le like
                // dd("like");
                $alike = $alikeRepository->findOneByUserAndArticle($user->getUserProfile(), $article);
                $manager->remove($alike);
                $manager->flush();

                return $this->json(["code" => "10", "counter" => count($article->getAlikes()), "message"=> "unlike"], 200);
            } else {
                // je like
                // dd("like");
                $alike = new Alike();
                $alike->setArticle($article);
                $alike->setUserProfile($user->getUserProfile()); // Warning ?? FIXME voir cast
                $manager->persist($alike);
                $manager->flush();

                return $this->json(["code" => "20", "counter" => count($article->getAlikes()), "message"=> "like"], 200);
            }
            

        }

        // return $this->redirectToRoute('article');
        
    }



    /**
     * @Route("/article/{id}/delete", name="article_delete")
     */
    public function article_delete(Article $article, EntityManagerInterface $manager): Response {

        $manager->remove($article);
        $manager->flush();


        return $this->redirectToRoute('article');
    }

    /**
     * @Route("/article/{id}/show", name="article_show")
     */
    // public function article_show(ArticleRepository $articleRepo, $id): Response
    public function article_show(Article $article, Request $request, EntityManagerInterface $manager): Response {
        // dd($article);
        $comment = new Comment();
        $formComment = $this->createForm(CommentType::class, $comment);

        $formComment->handleRequest($request);

        if($formComment->isSubmitted() && $formComment->isValid()){
            $comment->setCreatedAt(new \DateTime());
            $comment->setArticle($article);

            $manager->persist($comment);
            $manager->flush();

            $this->addFlash("success", "Votre commentaire a été ajouté");

            return $this->redirectToRoute('article_show', ['id' => $article->getId()]);
        }

        return $this->render('article/article.html.twig', [
            'article' => $article,
            'formComment' => $formComment->createView()

        ]);
    }

      /**
       * @Route("/article/{slug}", name="article_show_slug")
        */
    // public function article_show(ArticleRepository $articleRepo, $id): Response
    public function article_show_slug(Article $article, Request $request, EntityManagerInterface $manager): Response {
        // dd($article);
        $comment = new Comment();
        $formComment = $this->createForm(CommentType::class, $comment);

        $formComment->handleRequest($request);

        if($formComment->isSubmitted() && $formComment->isValid()){
            $comment->setCreatedAt(new \DateTime());
            $comment->setArticle($article);

            $manager->persist($comment);
            $manager->flush();

            $this->addFlash("success", "Votre commentaire a été ajouté");

            return $this->redirectToRoute('article_show', ['id' => $article->getId()]);
        }

        return $this->render('article/article.html.twig', [
            'article' => $article,
            'formComment' => $formComment->createView()

        ]);
    }

  


    //public function article_new(Request $request, EntityManager $manager): Response {
    //     // dump($request);

    //     if ($request->request->count() > 0) {
    //         $article = new Article();
    //         $article->setTitle($request->request->get('title'))
    //             ->setContent($request->request->get('content'))
    //             ->setAuthor($request->request->get('author'))
    //             ->setImage($request->request->get('image'))
    //             ->setCreatedAt(new \DateTime());

    //         $manager->persist($article);

    //         $manager->flush();

    //         return $this->redirectToRoute('article_show', ['id' => $article->getId()]);
    //     }

    //     return $this->render('article/new.html.twig', []);
    // }
}
