<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserProfile;
use App\Form\RegistrationType;
use App\Form\ChangePasswordType;
use App\Repository\UserRepository;
use App\Form\ForgottenPasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/register", name="app_security_register")
     */
    public function register(Request $request, EntityManagerInterface $manager, UserPasswordHasherInterface $encoder): Response
    {
        $user = new User();
        $formRegister = $this->createForm(RegistrationType::class, $user);

        $formRegister->handleRequest($request);
        if ($formRegister->isSubmitted() && $formRegister->isValid()) {
            $hash = $encoder->hashPassword($user, $user->getPassword());
            $user->setPassword($hash);

            $user->setIsValidate(false);
            $user->setValidationCode($this->generateValidationCode());

            $user->setRoles(['ROLE_USER']);

            // ajoute un profil vide
            $userProfile = new UserProfile();
            $userProfile->setUser($user);
            $userProfile->setCreatedAt(new \Datetime());
            $userProfile->setUpdatedAt(new \Datetime());

            $manager->persist($userProfile);

            $manager->persist($user);

            // j'enregitre le nouvel utilisateur
            $manager->flush();

            // j'appelle la route pour envoyer le mail
            $this->forward("\App\Controller\MailController::mailRegister", ['id' => $user->getId()]);

            $this->addFlash("success", "Vous êtes inscrit, merci. Vous recevoir un mail de validation.");

            // redirige vers la page de login
            return $this->redirectToRoute('app_security_login');
        }

        return $this->render('security/register.html.twig', [
            'form_register' => $formRegister->createView(),
        ]);
    }


    /**
     * @Route("/login", name="app_security_login")
     */
    public function login(): Response
    {
        return $this->render('security/login.html.twig', []);
    }

    /**
     * @Route("/logout", name="app_security_logout")
     */
    public function logout()
    {
    }

    /**
     * @Route("/validate_user/{code}", name="app_security_validate_user")
     */
    public function validateUser(UserRepository $userRepository, EntityManagerInterface $manager, $code): Response
    {
        // je vérifie si l'utilisateur existe via son code,  
        $user = $userRepository->findOneBy(['validationCode' => $code]);

        // s'il existe
        if ($user) {

            // je me valide 
            $user->setIsValidate(true);
            $user->getUserProfile()->setUpdatedAt(new \DateTime());

            // et on persiste
            $manager->persist($user);
            $manager->flush();
            $this->addFlash("success", "Votre compte est validé");
        } else {
            $this->addFlash("danger", "Une erreur est survenu, vérifier votre mail ou contacté un administrateur");
        }

        return $this->redirectToRoute('app_security_login', [], 302);
    }


    /**
     * @Route("/forgot", name="app_security_forgotten_password")
     */
    public function forgottenPass(Request $request, UserRepository $userRepository, EntityManagerInterface $manager): Response
    {
        // TODO afficher lien dans le formulaire de login
        $user = new User();
        // $form = $this->createForm(ForgottenPasswordType::class, $user);
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class)
            ->add('submit', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            // dd($request->request->get("form")['email']);
            // FIXME check forgot email
            $email = $request->request->get("form")['email'];
            $userExist = $userRepository->findOneBy(["email" => $email]);

            if ($userExist) {
                // on desactive le compte
                $userExist->setIsValidate(false);
                // on genere un nouveau code
                $userExist->setValidationCode($this->generateValidationCode());
                $userExist->getUserProfile()->setUpdatedAt(new \DateTime());
                $manager->persist($userExist);
                $manager->flush();

                $this->forward("\App\Controller\MailController::mailForgottenPass", ['id' => $user->getId()]);
                $this->addFlash("success", "Un mail d'être envoyé pour créer un nouveau mot de passe");
            } else {
                $this->addFlash("danger", "Email inconnue. Vérifiez votre adresse!");
            }
        }

        return $this->render('security/forgot_pass.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/create_password/{code}", name="app_security_create_password")
     */
    public function createPassword(UserRepository $userRepository, EntityManagerInterface $manager, UserPasswordHasherInterface $encoder, Request $request, $code): Response
    {

        $user = $userRepository->findOneBy(['validationCode' => $code]);
        if ($user) {
            $form = $this->createForm(ChangePasswordType::class, $user);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $hash = $encoder->hashPassword($user, $user->getPassword());
                $user->setPassword($hash);

                $user->setIsValidate(true);
                $user->getUserProfile()->setUpdatedAt(new \DateTime());

                $manager->persist($user);
                $manager->flush();
                $this->addFlash("success", "Votre Mot de passe à été modifié");

                return $this->redirectToRoute('app_security_login');
            }
            /*
        // je me valide 
            

            // et on persiste
           
        //*/

            return $this->render('security/create_pass.html.twig', [
                'form' => $form->createView()
            ]);
        } else {
            $this->addFlash("warning", "Action non autorisé");
            return $this->redirectToRoute("app_security_login");
        }
    }


    public function generateValidationCode()
    {
        return md5(uniqid());
    }
}
