<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MailController extends AbstractController
{
    /**
     * @Route("/mail/test", name="app_mail_test")
     */
    public function test(MailerInterface $mailer): Response
    {

        $email = (new Email())
        ->from('chezwam@mail.com')
        ->to("client@client.com")
        ->subject("test")
        ->text("Ceci est un mail de test")
        ->html("
            <h1>test</h1>
            <p>
                C'est un test mais en <strong>HTML</strong>
            </p>");

        $mailer->send($email);

        return new Response("Mail envoyé", Response::HTTP_OK);
    }

     /**
     * @Route("/mail/register/{id}", name="app_mail_register")
     */
    public function mailRegister(MailerInterface $mailer, User $user): Response
    {

        $url = $this->generateUrl("app_security_validate_user", [
            'code' => $user->getValidationCode()
        ],
        UrlGeneratorInterface::ABSOLUTE_URL);
        $email = (new Email())
        ->from('chezwam@mail.com')
        // ->to($user->getEmail())
        ->to("benaesan@msn.com")
        ->subject("Inscription sur le blog")
        ->text("Bienvenue {$user->getFirstname()} ,\n\nmerci de valider votre adresse. Ci-dessous le lien à copier-coller: \n 
        {$url}")
        ->html("
            <style>

            </style>
            <h1>Bienvenue {$user->getFirstname()}</h1>
            <p>
                merci de valider votre adresse. En cliquand sur le lien 
                <a href='{$url}'><button>ici</button></a>
            </p>
            
            <p>
            Ci-dessous le lien à copier-coller: <br>
            http://localhost:8000/validate_user/{$user->getValidationCode()}
            </p>");

        $mailer->send($email);

        return new Response("Mail envoyé", Response::HTTP_OK);
    }

     /**
     * @Route("/mail/forgotten_pass/{id}", name="app_mail_forgotten_pass")
     */
    public function mailForgottenPass(MailerInterface $mailer, User $user): Response
    {

        $url = $this->generateUrl("app_security_create_password", [
            'code' => $user->getValidationCode()
        ],
        UrlGeneratorInterface::ABSOLUTE_URL);
        $email = (new Email())
        ->from('chezwam@mail.com')
        ->to($user->getEmail())
        ->subject("Chnagement de mot de passe")
        ->text("Bonjour {$user->getFirstname()} ,\n\nmerci de cliquer sur le lien pour changer votre mot de passe.\n Ci-dessous le lien à copier-coller: \n 
        {$url}")
        ->html("
            <style>

            </style>
            <h1>Bonjour {$user->getFirstname()}</h1>
            <p>
            merci de cliquer sur le lien pour changer votre mot de passe. En cliquant sur le lien 
                <a href='{$url}'><button>ici</button></a>
            </p>
            
            <p>
            Ci-dessous le lien à copier-coller: <br>
            {$url}
            </p>");

        $mailer->send($email);


        return new Response("Mail envoyé", Response::HTTP_OK);
    }
}
