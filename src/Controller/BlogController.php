<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Form\BlogType;
use Doctrine\ORM\EntityManager;
use App\Repository\BlogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(): Response
    {
        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
        ]);
    }

     /**
     * @Route("/admin/blog/slogan/manager", name="blog_slogan_manager")
     */
    public function blogSloganManager(Request $request, EntityManagerInterface $manager, BlogRepository $blogRepository): Response
    {
        // $blog = new Blog();
        $blogs = $blogRepository->findAll();

        // si pas d'instance, j'en créé une nouvelle
        if($blogs == null) {
            $blog = new Blog();
        } else {
            $blog = $blogs[0];
        }

        $form = $this->createForm(BlogType::class, $blog);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
           
            $manager->persist($blog);
            $manager->flush();

            $this->addFlash("success", "Votre slogan a été ajouté"); 

            return $this->redirectToRoute('blog');
        }

        return $this->render('blog/blog_config.html.twig', [
            'controller_name' => 'BlogController',
            'form' => $form->createView()
        ]);
    }
}
