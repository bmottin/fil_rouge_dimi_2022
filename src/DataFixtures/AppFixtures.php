<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Status;
use App\Entity\Article;
use App\Entity\BlogRole;
use App\Entity\Comment;
use App\Entity\Category;
use App\Entity\UserProfile;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AppFixtures extends Fixture
{

    private UserPasswordHasherInterface $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }


   
    
    public function load(ObjectManager $manager): void
    {

        $strCom = Array(
            "J'étais prêt à tourner la page, mais c'est la page qui ne veut pas se tourner.",
            "Écoute... Je n'aime pas faire la morale, mais je vais te donner un conseil qui te servira à jamais. Dans la vie tu rencontreras beaucoup de cons.",
            "Le coeur d'une femme est un océan de secrets.",
            "J'ai des mains faites pour l'or et elles sont dans la merde.",
            "Si je te dis de me parler d'art, tu vas me balancer un condensé de tous les livres sur le sujet. Michel-Ange, tu sais plein de trucs sur lui. Sur son oeuvre, sur ses choix politiques, sur lui et sur le pape, ses tendances sexuelles, tout le bazar quoi. Mais je parie que ce qu'on respire dans la Chapelle Sixtine, son odeur, tu connais pas. Tu ne peux pas savoir ce que c'est que de lever les yeux sur le magnifique plafond. Tu sais pas."
        );

        $strAuth = [
            "Will Hunting",
            "Immortan Joe",
            "Ken le survivant",
            "Pierre Parker",
            "JJJ",            
        ];

        $blogRole = new BlogRole();
        $blogRole->setInternalName("ROLE_USER");
        $blogRole->setLabel("Utilisateur");

        $blogRole1 = new BlogRole();
        $blogRole1->setInternalName("ROLE_MODERATOR");
        $blogRole1->setLabel("Moderateur");

        $blogRole2 = new BlogRole();
        $blogRole2->setInternalName("ROLE_EDITOR");
        $blogRole2->setLabel("Editor");

        $blogRole3 = new BlogRole();
        $blogRole3->setInternalName("ROLE_ADMIN");
        $blogRole3->setLabel("Admin");

        $manager->persist($blogRole);
        $manager->persist($blogRole1);
        $manager->persist($blogRole2);
        $manager->persist($blogRole3);
        
        /*
            User
        */

        $user1 = new User();
        $user1->setEmail('u1@mail.com');
        $user1->setLastname('Parker');
        $user1->setFirstname('Pierre');
        $user1->setPassword($this->encoder->hashPassword($user1, 'admin123'));
        $user1->setRoles(['ROLE_USER']);
        $user1->setIsValidate(true);
        $user1->setHasRgpd(true);



        $userProfile1 = new UserProfile();
        $userProfile1->setUser($user1);
        $userProfile1->setCreatedAt(new \Datetime());
        $userProfile1->setUpdatedAt(new \Datetime());


        $manager->persist($user1);
        $manager->persist($userProfile1);

        $user2 = new User();
        $user2->setEmail('u2@mail.com');
        $user2->setLastname('Paul');
        $user2->setFirstname('Jacques');
        $user2->setPassword($this->encoder->hashPassword($user2, 'admin123'));
        $user2->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
        $user2->setIsValidate(true);
        $user2->setHasRgpd(false);
        
        $userProfile2 = new UserProfile();
        $userProfile2->setUser($user2);
        $userProfile2->setCreatedAt(new \Datetime());
        $userProfile2->setUpdatedAt(new \Datetime());
        
        $manager->persist($user2);
        $manager->persist($userProfile2);

        $user3 = new User();
        $user3->setEmail('u3@mail.com');
        $user3->setLastname('Gordon');
        $user3->setFirstname('Flash');
        $user3->setPassword($this->encoder->hashPassword($user3, 'admin123'));
        $user3->setRoles(['ROLE_USER', 'ROLE_AUTHOR']);
        $user3->setIsValidate(true);
        $user3->setHasRgpd(true);

        
        $userProfile3 = new UserProfile();
        $userProfile3->setUser($user3);
        $userProfile3->setCreatedAt(new \Datetime());
        $userProfile3->setUpdatedAt(new \Datetime());

        $manager->persist($userProfile3);
        $manager->persist($user3);

        /*
            Category
        */
        $categ1 = new Category();
        $categ1->setLabel("Informatique");

        $categ2 = new Category();
        $categ2->setLabel("Materiel");

        $manager->persist($categ1);
        $manager->persist($categ2);

        /*
            STATUS
        */
        $stat1 = new Status();
        $stat1->setLabel("Brouillon");
        $stat1->setInternalName('draft');
        $manager->persist($stat1);

        $stat2 = new Status();
        $stat2->setLabel("Publié");
        $stat2->setInternalName('publish');
        $manager->persist($stat2);

        $stat3 = new Status();
        $stat3->setLabel("A supprimer");
        $stat3->setInternalName('to_delete');
        $manager->persist($stat3);

        // $product = new Product();
        // $manager->persist($product);
        $article = new Article();
        $article->setTitle("WebStorm 2021.3 est disponible");
        $article->setAuthor($user2);
        $article->setContent(" WebStorm 2021.3, la deuxième mise à jour majeure de l'année de l'EDI de JetBrains pour les développeurs JavaScript, est disponible. Cette version vient avec un bon lot de nouvelles fonctionnalités, incluant la prise en charge des membres de classes privées ES2022, de nouvelles fonctionnalités pour le développement à distance, une meilleure prise en charge des monorepos, le protocole de serveur de langage Deno, la possibilité de fractionner la fenêtre d'outils Run, et bien plus encore.");
        //$article->setImage("http://via.placeholder.com/350x150");
        $article->setCreatedAt(new \DateTime($this->getRandomDate()));
        $article->setUpdatedAt(new \DateTime($this->getRandomDate()));
        $article->setStatus($stat1);
        $article->setCategory($categ1);

        $manager->persist($article);

        $com1 = new Comment();
        $com1->setAuthor("Benoit");
        $com1->setContent("Blabla c'est très intéressant");
        $com1->setArticle($article);
        $com1->setCreatedAt(new \DateTime($this->getRandomDate()));
        $manager->persist($com1);

        $users = [$user2, $user3];

        for ($i = 0; $i < 10; $i++) {

            $chiffre = rand(0, count($users)-1);

            $date = $this->getRandomDate();

            $article2 = new Article();
            $article2->setTitle("Titre de l'article " . ($i + 1));
            $article2->setAuthor($users[$chiffre]);
            $article2->setContent("<h3>Lorem Ipsum</h3>Lorem ipsum dolor, sit amet <strong>consectetur adipisicing elit</strong>. Officia esse architecto temporibus odio eligendi aut enim veritatis provident asperiores dolores repudiandae, earum quae, dolor natus tempora, praesentium fugit distinctio? Molestias, enim libero excepturi a voluptas impedit beatae! Quidem possimus iure voluptatum, reprehenderit debitis nisi culpa officia facilis repudiandae enim, modi ab amet. Perspiciatis deleniti error deserunt accusantium voluptatum dicta unde repellat, officia eos nisi, atque fugit! Deserunt placeat corrupti dignissimos similique porro! Officiis cupiditate, fugiat deleniti corrupti nesciunt at veritatis ipsa, labore soluta aspernatur nam dolores quod voluptatibus repellendus quaerat, facere ut obcaecati quisquam provident earum? Eum fugit harum inventore. Illo laboriosam placeat id, velit quae voluptas eaque fuga quia natus iste corporis ab necessitatibus autem aut. Veniam nostrum placeat unde, ad molestiae dolorum libero eos, voluptatem saepe distinctio suscipit maiores? Alias debitis veniam voluptates laborum praesentium dolorum itaque eius error cupiditate molestias, fugiat quo consectetur enim laudantium. Illum, voluptatibus?");
            //$article2->setImage("http://via.placeholder.com/350x150");
            $article2->setCreatedAt(new \DateTime($date));
            $article2->setUpdatedAt(new \DateTime($date));
            $article2->setStatus($stat2);
            $article2->setCategory($categ2);

            for ($i = 0; $i < rand(0, 10); $i++) {
                $com1 = new Comment();
                $com1->setAuthor($strAuth[rand(0,count($strAuth)-1)]);
                $com1->setContent($strCom[rand(0,count($strCom)-1)]);
                $com1->setArticle($article2);
                $com1->setCreatedAt(new \DateTime());
                $manager->persist($com1);
            }


            $manager->persist($article2);
        }
        


        $manager->flush();
    }


    function getRandomDate() {
        $d = random_int(1, 28);
        $m = random_int(1, 12);
        $y = random_int(2020, 2022);

        $h = random_int(0, 23);
        $i = random_int(00, 59);

        return $y . '-'.$m.'-'.$d . ' ' . $h . ':' . $i;
    }
}
